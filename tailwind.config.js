const animate = require('tailwindcss-animate');

export default {
	content: [
        `/components/**/*.{vue,js,ts}`,
        `/layouts/**/*.vue`,
        `/pages/**/*.vue`,
        `/composables/**/*.{js,ts}`,
        `/plugins/**/*.{js,ts}`,
        `/utils/**/*.{js,ts}`,
        `/App.{js,ts,vue}`,
        `/app.{js,ts,vue}`,
        `/Error.{js,ts,vue}`,
        `/error.{js,ts,vue}`,
        `/app.config.{js,ts}`,
	],
	darkMode: ['class'],
	safelist: ['dark'],
	prefix: '',
	theme: {
		container: {
			center: true,
			padding: {
				'DEFAULT': '1rem',
				'sm': '1rem',
				'md': '1.5rem',
				'lg': '2rem',
				'xl': '6.25rem',
				'2xl': '11.25rem',
			},
		},
		extend: {
			screens: {
				'2xl': '1440px',
			},
			colors: {
				border: 'hsl(var(--border))',
				input: 'hsl(var(--input))',
				ring: 'hsl(var(--ring))',
				background: 'hsl(var(--background))',
				foreground: 'hsl(var(--foreground))',
				primary: {
					DEFAULT: 'hsl(var(--primary))',
					foreground: 'hsl(var(--primary-foreground))',
				},
				secondary: {
					DEFAULT: 'hsl(var(--secondary))',
					foreground: 'hsl(var(--secondary-foreground))',
				},
				destructive: {
					DEFAULT: 'hsl(var(--destructive))',
					foreground: 'hsl(var(--destructive-foreground))',
				},
				muted: {
					DEFAULT: 'hsl(var(--muted))',
					foreground: 'hsl(var(--muted-foreground))',
				},
				accent: {
					DEFAULT: 'hsl(var(--accent))',
					foreground: 'hsl(var(--accent-foreground))',
				},
				popover: {
					DEFAULT: 'hsl(var(--popover))',
					foreground: 'hsl(var(--popover-foreground))',
				},
				card: {
					DEFAULT: 'hsl(var(--card))',
					foreground: 'hsl(var(--card-foreground))',
				},
			},
			borderRadius: {
				xl: 'calc(var(--radius) + 4px)',
				lg: 'var(--radius)',
				md: 'calc(var(--radius) - 2px)',
				sm: 'calc(var(--radius) - 4px)',
			},
			keyframes: {
				'accordion-down': {
					from: { height: 0 },
					to: { height: 'var(--radix-accordion-content-height)' },
				},
				'accordion-up': {
					from: { height: 'var(--radix-accordion-content-height)' },
					to: { height: 0 },
				},
				'collapsible-down': {
					from: { height: 0 },
					to: { height: 'var(--radix-collapsible-content-height)' },
				},
				'collapsible-up': {
					from: { height: 'var(--radix-collapsible-content-height)' },
					to: { height: 0 },
				},
			},
			animation: {
				'accordion-down': 'accordion-down 0.2s ease-out',
				'accordion-up': 'accordion-up 0.2s ease-out',
				'collapsible-down': 'collapsible-down 0.2s ease-in-out',
				'collapsible-up': 'collapsible-up 0.2s ease-in-out',
			},
		},
	},
	plugins: [animate],
};
