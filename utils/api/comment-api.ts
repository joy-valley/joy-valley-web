import { useClientRequest, useServerRequest } from '~/composables/request';
import type { Result } from '~/utils/api/type';
import type { User } from '~/utils/api/user-api';

export interface CommentList {
	list: Comment[];
	total: number;
	pageNo: number;
	pageSize: number;
}

export interface CommentChildList {
	list: CommentChild[];
	total: number;
	pageNo: number;
	pageSize: number;
}

export interface Comment {
	commentId: number;
	content: string;
	status: string;
	createDate: string;
	updateDate: string;
	user: User;
	children: CommentChild[];
	childrenCount: number;
}

export interface CommentChild {
	commentId: number;
	content: string;
	createDate: string;
	replyUser: User;
	status: string;
	updateDate: string;
	user: User;
}

export const commentApi = {
	/**
     * 发送评论
     */
	sendComment: (articleId: number, content: string, parentId?: number) => {
		return useClientRequest<Result<unknown>>({
			url: `/api/v1/comment/send`,
			body: {
				articleId,
				content,
				parentId,
			},
			options: {
				method: 'post',
			},
		});
	},
	/**
     * 按文章id获取评论列表
     */
	queryCommentListServer: (articleId: number, pageNo: number = 1, pageSize: number = 10) => {
		return useServerRequest<Result<CommentList>>({
			url: `/api/v1/comment/list/${articleId}`,
			query: {
				pageNo,
				pageSize,
			},
		});
	},
	// 同上客户端获取
	queryCommentListClient: (articleId: number, pageNo: number = 1, pageSize: number = 10) => {
		return useClientRequest<Result<CommentList>>({
			url: `/api/v1/comment/list/${articleId}`,
			query: {
				pageNo,
				pageSize,
			},
		});
	},
	// 按父评论id获取子评论列表，客户端获取
	queryChildrenListClient: (commentId: number, pageNo: number = 1, pageSize: number = 10) => {
		return useClientRequest<Result<CommentChildList>>({
			url: `/api/v1/comment/children/${commentId}`,
			query: {
				pageNo,
				pageSize,
			},
		});
	},
};
