export interface Tag {
	tag_id: number;
	tag_name: string;
	tag_description: string;
	tag_slug: string;
	create_date: string;
	update_date: string;
}
