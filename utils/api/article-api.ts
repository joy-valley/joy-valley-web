import { useClientRequest, useServerRequest } from '~/composables/request';
import type { Result } from '~/utils/api/type';
import type { Category } from '~/utils/api/category-api';
import type { Tag } from '~/utils/api/tag-api';
import type { User } from '~/utils/api/user-api';

export interface ArticleList {
	list: Article[];
	total: number;
	pageNo: number;
	pageSize: number;
}

export interface Article {
	article_id: number;
	article_title: string;
	article_description: string;
	article_content?: string;
	cover_src: string;
	article_views: string;
	article_comment_count: string;
	article_like_count: string;
	article_status: string;
	article_public_date: string;
	create_date: string;
	update_date: string;
	author: User;
	tags: Tag[];
	categories: Category[];
}

export const articleApi = {
	/**
     * 客户端获取文章列表
     * @param pageNo 页数
     * @param pageSize 每页数量
     */
	getArticleList: (pageNo: number = 1, pageSize: number = 10) => {
		return useClientRequest<Result<ArticleList>>({
			url: `/api/v1/article/list`,
			query: {
				pageNo: pageNo,
				pageSize: pageSize,
			},
		});
	},
	/**
     * 服务端获取文章列表
     * @param pageNo 页数
     * @param pageSize 每页数量
     */
	getArticleListServer(pageNo: number = 1, pageSize: number = 10) {
		return useServerRequest<Result<ArticleList>>({
			url: `/api/v1/article/list`,
			query: {
				pageNo: pageNo,
				pageSize: pageSize,
			},
		});
	},
	/**
     * 服务端获取文章详情
     * @param id 文章id
     */
	getArticleServer: (id: number) => {
		return useServerRequest<Result<Article>>({
			url: `/api/v1/article/${id}`,
		});
	},
};
