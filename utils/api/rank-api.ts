import { useServerRequest } from '~/composables/request';
import type { Result } from '~/utils/api/type';

export interface RealTimeRank {
	article_id: number;
	article_title: string;
	article_views: number;
}

export const rankApi = {
	/**
     * 服务端获取实时排行榜
     */
	getRealTimeRankingServer: () => {
		return useServerRequest<Result<RealTimeRank[]>>({
			url: `/api/v1/ranking/realTime`,
		});
	},
};
