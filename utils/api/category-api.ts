import { useServerRequest } from '~/composables/request';
import type { Result } from '~/utils/api/type';
import type { Article } from '~/utils/api/article-api';

export interface Category {
	category_id: number;
	category_name: string;
	category_description: string;
	category_slug: string;
	create_date: string;
	update_date: string;
}

export const categoryApi = {
	/**
     * 服务端获取分类列表
     */
	getCategoryListServer: (pageNo: number = 1, pageSize: number = 10) => {
		return useServerRequest<Result<Category[]>>({
			url: `/api/v1/article/category/list`,
			query: {
				pageNo,
				pageSize,
			},
		});
	},
	/**
     * 服务端按id获取分类信息
     */
	getCategoryInfoServer: (id: number) => {
		return useServerRequest<Result<Category>>({
			url: `/api/v1/article/category/${id}`,
		});
	},
	/**
     * 服务端按id获取分类下的文章
     */
	getCategoryArticleServer: (id: number, pageNo: number = 1, pageSize: number = 10) => {
		return useServerRequest<Result<Article[]>>({
			url: `/api/v1/article/category/${id}/article`,
			query: {
				pageNo,
				pageSize,
			},
		});
	},
};
