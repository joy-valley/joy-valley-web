export interface TopNavGroup {
	title: string;
	items: TopNav[];
}

export interface TopNav {
	title: string;
	href: string;
	description: string;
}
