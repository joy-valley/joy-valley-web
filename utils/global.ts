export function getScreenSize(): string {
	if (window.innerWidth >= 1440) {
		return '2xl';
	}
	else if (window.innerWidth >= 1024) {
		return 'xl';
	}
	else if (window.innerWidth >= 768) {
		return 'lg';
	}
	else if (window.innerWidth >= 640) {
		return 'md';
	}
	else {
		return 'sm';
	}
}
