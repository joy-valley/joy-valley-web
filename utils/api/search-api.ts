import { useClientRequest, useServerRequest } from '~/composables/request';
import type { Result } from '~/utils/api/type';
import type { Article } from '~/utils/api/article-api';

export interface ArticleSearchResult {
	list: Article[];
	total: number;
	pageNo: number;
	pageSize: number;
}

export const searchApi = {
	/**
     * 服务端搜索
     */
	searchArticleServer: (keyword: string, pageNo: number = 1, pageSize: number = 10) => {
		return useServerRequest<Result<ArticleSearchResult>>({
			url: `/api/v1/search`,
			query: {
				keyword: keyword,
				pageNo: pageNo,
				pageSize: pageSize,
			},
		});
	},
	/**
     * 客户端搜索
     */
	searchArticle: (keyword: string, pageNo: number = 1, pageSize: number = 10) => {
		return useClientRequest<Result<ArticleSearchResult>>({
			url: `/api/v1/search`,
			query: {
				keyword: keyword,
				pageNo: pageNo,
				pageSize: pageSize,
			},
		});
	},
};
