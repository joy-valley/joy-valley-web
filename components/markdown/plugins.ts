import type { BytemdPlugin } from 'bytemd';
import { visit } from 'unist-util-visit';
import Slugger from 'github-slugger';
import type { Ref } from 'vue';

export interface Toc {
	value: string;
	href: string;
	depth: 1 | 2 | 3 | 4 | 5 | 6;
}

export function tocPlugin(refToc: Ref<Toc[] | undefined>): BytemdPlugin {
	return {
		// eslint-disable-next-line
        remark: processor => processor.use(() => (tree: any, _: any) => {
			const toc: Toc[] = [];

			// eslint-disable-next-line
            function stringifyHeading(e: any) {
				let result = '';
				visit(e, 'text', (node) => {
					result += node.value;
				});
				return result;
			}

			const slugger = new Slugger();
			slugger.reset();

			// 循环所有的heading
			visit(tree, 'heading', (node, _, parent) => {
				if (!parent) return;

				const title = stringifyHeading(node);

				if (!node.data) node.data = {};
				if (!node.data.hProperties) node.data.hProperties = {};

				node.data.hProperties.id = slugger.slug(title);

				toc.push({
					value: title,
					href: `#user-content-${node.data.hProperties.id}`,
					depth: node.depth,
				});
			});
			refToc.value = toc;
		}),
	};
}
