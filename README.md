# Nuxt Blog

看看 [Nuxt 3 文档](https://nuxt.com/docs/getting-started/introduction) 以了解更多信息。

## 设置

确保安装依赖项:

```bash
npm install -g pnpm
pnpm install
```

## 开发服务器

启动开发服务器 `http://localhost:3000`:

```bash
pnpm run dev
```

## 编译

编译应用程序：

```bash
pnpm run build
```

本地预览生产版本:

```bash
pnpm run preview
```

查看 [部署文档](https://nuxt.com/docs/getting-started/deployment) 了解更多信息。

## 许可证

本项目以GNU通用公共许可证v3.0授权，详情请参见【许可证】(License)文件。