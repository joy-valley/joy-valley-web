/**
 * api接口统一返回的数据类型
 */
export interface Result<T> {
	code: number;
	message: string | string[];
	data: T;
}
