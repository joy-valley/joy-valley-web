// https://nuxt.com/docs/api/configuration/nuxt-config

export default defineNuxtConfig({
	app: {
		pageTransition: { name: 'blur', mode: 'out-in' },
		layoutTransition: { name: 'blur', mode: 'out-in' },
		head: {
			viewport: 'width=device-width, initial-scale=1.0, user-scalable=no',
		},
	},
	css: [
		'assets/css/global.pcss',
	],
	devtools: { enabled: true },
	modules: [
		'@pinia/nuxt',
		'shadcn-nuxt',
		'@nuxtjs/tailwindcss',
		'@vueuse/nuxt',
		'@nuxt/eslint',
	],
	shadcn: {
		prefix: '',
		componentDir: './components/ui', // 组件所在的目录
	},
	eslint: {
		config: {
			stylistic: {
				indent: 'tab',
				semi: true,
			},
		},
	},
	plugins: [],
});
