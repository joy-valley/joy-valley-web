import { useClientRequest } from '~/composables/request';
import type { Result } from '~/utils/api/type';

export interface User {
	id: number;
	username: string;
	password: string;
	nickName: string;
	email: string;
	headPic: string;
	phoneNumber: string;
	isFrozen: boolean;
	isAdmin: boolean;
	createTime: string;
	updateTime: string;
}

export interface registerCaptcha {
	address: string;
}

export interface UserRegister {
	password: string;
	email: string;
	captcha: string;
}

export interface UserLogin {
	email: string;
	password: string;
}

export interface UserInfo {
	avatar: null | string;
	birthdayDate: string;
	currentRole: CurrentRole;
	email: string;
	nickName: string;
	permissionIds: string[];
	phoneNumber: string;
	roles: Role[];
	userId: number;
	username: string;
}

export interface CurrentRole {
	id: number;
	roleCode: string;
	roleName: string;
}

export interface Role {
	id?: number;
	roleCode?: string;
	roleName?: string;
}

export interface RefreshToken {
	accessToken: string;
	refreshToken: string;
}

export const userApi = {
	/**
     * 客户端发送验证码
     * @param address 邮箱地址
     */
	sendCaptcha: (address: string) => {
		return useClientRequest<Result<registerCaptcha>>({
			url: `/api/v1/user/register-captcha?address=${address}`,
		});
	},
	/**
     * 客户端发送用户注册
     * @param user 用户注册信息
     */
	userRegister: (user: UserRegister) => {
		return useClientRequest<Result<UserRegister>>({
			url: `/api/v1/user/register`,
			body: user,
			options: {
				method: 'post',
			},
		});
	},
	/**
     * 客户端发送用户登录
     * @param user 用户登录信息
     */
	userLogin: (user: UserLogin) => {
		return useClientRequest<Result<RefreshToken>>({
			url: `/api/v1/auth/login`,
			body: user,
			options: {
				method: 'post',
			},
		});
	},
	/**
     * 客户端获取用户信息
     */
	getUserInfo: () => {
		return useClientRequest<Result<UserInfo>>({
			url: `/api/v1/user/userInfo`,
		});
	},
	logout: () => {
		return useClientRequest<Result<void>>({
			url: `/api/v1/auth/logout`,
			options: {
				method: 'post',
			},
		});
	},
};
